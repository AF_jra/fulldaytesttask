package de.appsfactory.android.blueprint.ui.general

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment

abstract class BaseFragment<Model : BaseViewModel, Binding : ViewDataBinding> : Fragment() {
    protected abstract val viewModel: Model
    protected abstract val layoutId: Int
    protected abstract val viewModelResId: Int

    protected lateinit var binding: Binding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return DataBindingUtil.inflate<Binding>(inflater, layoutId, container, false).also {
            binding = it
            binding.lifecycleOwner = viewLifecycleOwner
            binding.setVariable(viewModelResId, viewModel)
        }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        internalBindings()
        doBindings()
    }

    private fun internalBindings() {
        // do nothing, for now :)
    }

    abstract fun doBindings()
}
