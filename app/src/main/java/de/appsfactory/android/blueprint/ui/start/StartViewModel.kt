package de.appsfactory.android.blueprint.ui.start

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import de.appsfactory.android.blueprint.ui.general.BaseViewModel
import java.math.BigInteger

private const val START_VALUE = 0

class StartViewModel : BaseViewModel() {

    private var internalCounter = START_VALUE

    private val _counter = MutableLiveData<BigInteger>()
    val counter: LiveData<BigInteger> = _counter

    fun increaseCounter() {
        _counter.postValue(calculate(internalCounter++))
    }

    private fun calculate(count: Int): BigInteger {
        return when {
            count <= 0 -> 0.toBigInteger()
            count <= 2 -> 1.toBigInteger()
            count % 2 == 0 -> calculate(count - 1) + calculate(count - 3)
            else -> calculate(count - 1) + calculate(count - 2)
        }

    }
}
