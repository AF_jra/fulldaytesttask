package de.appsfactory.android.blueprint.ui.general

import androidx.lifecycle.ViewModel

abstract class BaseViewModel : ViewModel()
