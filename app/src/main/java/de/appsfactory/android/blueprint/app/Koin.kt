package de.appsfactory.android.blueprint.app

import de.appsfactory.android.blueprint.ui.start.StartViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

object Koin {
    private val myModule = module {
        viewModel { StartViewModel() }
    }

    val modules = listOf(
        myModule
    )
}
