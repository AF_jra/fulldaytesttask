package de.appsfactory.android.blueprint.ui.start

import androidx.lifecycle.Observer
import de.appsfactory.android.blueprint.BR
import de.appsfactory.android.blueprint.R
import de.appsfactory.android.blueprint.databinding.FragmentStartBinding
import de.appsfactory.android.blueprint.ui.general.BaseFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class StartFragment : BaseFragment<StartViewModel, FragmentStartBinding>() {

    override val viewModelResId = BR.viewModel
    override val viewModel: StartViewModel by viewModel()
    override val layoutId: Int = R.layout.fragment_start

    companion object {
        fun newInstance() = StartFragment()
    }

    override fun doBindings() {
        viewModel.counter.observe(
            viewLifecycleOwner,
            Observer { binding.textViewCounter.text = it.toString() }
        )
        binding.button.setOnClickListener { viewModel.increaseCounter() }
    }
}
